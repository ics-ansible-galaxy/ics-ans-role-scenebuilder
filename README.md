# ics-ans-role-scenebuilder

Ansible role to install [Scene Builder](https://gluonhq.com/products/scene-builder/).

## Requirements

- ansible >= 2.7
- molecule >= 2.6

## Role Variables

```yaml
---
scenebuilder_version: 16.0.0
scenebuilder_pkg: "https://artifactory.esss.lu.se/artifactory/swi-pkg/Scenebuilder/SceneBuilder-{{ scenebuilder_version }}.rpm"

```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-scenebuilder
```

## License

BSD 2-clause
