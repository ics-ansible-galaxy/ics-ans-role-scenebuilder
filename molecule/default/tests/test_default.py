import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_scenebuilder_install_complete(host):
    assert host.package("scenebuilder").is_installed
    assert host.package("scenebuilder").version == "16.0.0"
